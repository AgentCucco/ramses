--Thank you pettankon, Echo, and Jean-alphonse for the help! And to everyone who I missed!

local Ramsesmod = RegisterMod("Ramses", 1.0) -- Change the part in quotes to match your mod name
local RamsesPlayer = Isaac.GetPlayerTypeByName("Ramses")
local game = Game()
local sound = SFXManager()
local PicaItem = Isaac.GetItemIdByName("Pica") -- Get the item id by name
local RamsesHead = Isaac.GetEntityVariantByName("Hungry Ramses")
local RamsesPullEffect = Isaac.GetEntityVariantByName("Ramses Pulling Effect")

if not __eidItemDescriptions then __eidItemDescriptions = {} end
__eidItemDescriptions[PicaItem] = "Suck everything in the room and get +0.08 damage for each pickup you consume. #\3 Levels up the item if [Drop] is pressed when you hit the cap. #Tier 1: Normal suck. #Tier 2: Grants a speed up. #Tier 3: Allows you to consume Troll Bombs and black flies. #Tier 4: Grants poison tears, and -2 tear delay when hitting the cap."

------= Save data =------
local json = require("json")
local PerR_Data = { -- Persistant Ramses Data / each table represents a player.
	{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
	{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
	{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
	{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
}

function Ramsesmod:SaveModData()
	Ramsesmod:SaveData(json.encode(PerR_Data))
end

Ramsesmod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, fromSave)
	if not fromSave then
		PerR_Data = {
			{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
			{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
			{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
			{CurrentRamsesTier = 1, SuccedBonus = 0, MaxAmount = 10, MinAmount = 0},
		}
		Ramsesmod:SaveModData()
	else
		if Isaac.HasModData(Ramsesmod) then PerR_Data = json.decode(Ramsesmod:LoadData()) end
	end
		
	-- Updates the player's stats when the game starts (only player 0).
	local player = game:GetPlayer(0)
	
	player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_SPEED | CacheFlag.CACHE_TEARFLAG | CacheFlag.CACHE_TEARCOLOR | CacheFlag.CACHE_FIREDELAY)
	player:EvaluateItems()
end)

Ramsesmod:AddCallback(ModCallbacks.MC_PRE_GAME_EXIT, function(_, shouldSave)
	if shouldSave then
		Ramsesmod:SaveModData()
	end
end)

------= True Co-op Interface =------
local RamsesPlayerData = {
	Name = "Ramses",
	Type = PlayerType.PLAYER_ISAAC,
	SelectionGfx = "gfx/truecoop/char_portraits/rames_portrait.png",
	GhostCostume = Isaac.GetCostumeIdByPath("gfx/characters/ghost_ramses.anm2"),
	MaxHearts = 2,
	Hearts = 2,
	SoulHearts = 4,
	BlackHearts = 0,
	Pill = true,
	PillEffect = PillEffect.PILLEFFECT_GULP,
	Trinket = TrinketType.TRINKET_MATCH_STICK,
	Items = {PicaItem},
	OnStart = function(player)
		local sprite = player:GetSprite()
		sprite:Load("gfx/characters/character_ramses.anm2", true)
		sprite:LoadGraphics()
		
		player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_SPEED | CacheFlag.CACHE_TEARFLAG | CacheFlag.CACHE_TEARCOLOR | CacheFlag.CACHE_FIREDELAY)
		player:EvaluateItems()
	end,
	--OnChangeCharacter = Function, -- Clear up data, remove costumes, etc. Passes EntityPlayer.
	--PreDeath = Function, -- If your mod meddles with revival, return false in this function to stop players becoming ghosts. Passes EntityPlayer.
	AllowHijacking = true,
	ActualType = RamsesPlayer,
	FetusSprite = "gfx/truecoop/player_fetus/body_ramses.anm2",
	Coins = 0,
	Keys = 0,
	Bombs = 1,
	BossPortrait = "gfx/ui/boss/playerportrait_ramses.png",
	BossName = "gfx/ui/boss/playername_ramses.png",
	GhostPortrait = "gfx/ui/boss/playerportrait_ramses_ghost.png",
	GhostName = "gfx/ui/boss/playername_ramses_ghost.png"
}

local function onTrueCoopInit()
	InfinityTrueCoopInterface.AddCharacter(RamsesPlayerData)
	InfinityTrueCoopInterface.AddCharacterToWheel("Ramses")
	InfinityTrueCoopInterface.AssociatePlayerTypeName(RamsesPlayer, "Ramses")
end

if InfinityTrueCoopInterface then
    onTrueCoopInit()
else
    if not __infinityTrueCoop then
        __infinityTrueCoop = {}
    end

    __infinityTrueCoop[#__infinityTrueCoop + 1] = onTrueCoopInit
end

------= Actual Mod =------
local Ramses = {
	DAMAGE = 0.5,
	SPEED = 0.5,
	LUCK = 1,
}

local RamsesTiers = {
	[1] = {MinAmount = 0, MaxAmount = 10},
	[2] = {MinAmount = 5, MaxAmount = 20},
	[3] = {MinAmount = 10, MaxAmount = 30},
	[4] = {MinAmount = 15, MaxAmount = 35},
}

function Ramsesmod:UpdateStats(player, cacheFlag)
	local data = player:GetData()
	
	--If the player is Ramses, update his stats.
	if player:GetName() == "Ramses" then
		--=?= This updates the stats every cache update. If you only update them once, the player will lose those stats on the next update, instead of keeping them.
		if cacheFlag == CacheFlag.CACHE_DAMAGE then
			player.Damage = player.Damage + Ramses.DAMAGE
		end
		if cacheFlag == CacheFlag.CACHE_SPEED then
			player.MoveSpeed = player.MoveSpeed - Ramses.SPEED
		end
		if cacheFlag == CacheFlag.CACHE_LUCK then
			player.Luck = player.Luck + Ramses.LUCK
		end
	end
	
	-- Manage the Pica bonus.
	if player:HasCollectible(PicaItem) or player:GetPlayerType() == RamsesPlayer then
		-- To register the player index.
		if data.RP_Index == nil and data.TrueCoop and data.TrueCoop.PlayerListIndex then
			data.RP_Index = data.TrueCoop.PlayerListIndex
		end
		data.RP_Index = data.RP_Index or 1 -- data.RamsesPlayerIndex
		
		if cacheFlag == CacheFlag.CACHE_DAMAGE then ---Make Ramses more red the more he succ
			local percentage = (PerR_Data[data.RP_Index].SuccedBonus / PerR_Data[data.RP_Index].MaxAmount) * 0.75
			if PerR_Data[data.RP_Index].SuccedBonus == PerR_Data[data.RP_Index].MaxAmount then percentage = 1 end
		
			player.Damage = player.Damage + (0.08 * PerR_Data[data.RP_Index].SuccedBonus)
			
			if PerR_Data[data.RP_Index].SuccedBonus < 35 and PerR_Data[data.RP_Index].SuccedBonus < PerR_Data[data.RP_Index].MaxAmount then
				player:SetColor(Color(1, 1 - percentage, 1 - percentage, 1, 0, 0, 0), -1, 50, false, false)
			end
		end
		
		-- If the current Ramses tier is 2 or higher.
		if cacheFlag == CacheFlag.CACHE_SPEED and PerR_Data[data.RP_Index].CurrentRamsesTier >= 2 then
			player.MoveSpeed = player.MoveSpeed + 0.3
		end
		
		-- If the current Ramses tier is 4 or higher.
		if cacheFlag == CacheFlag.CACHE_TEARFLAG and PerR_Data[data.RP_Index].CurrentRamsesTier >= 4 then
			player.TearFlags = player.TearFlags | TearFlags.TEAR_POISON
		end
		
		if cacheFlag == CacheFlag.CACHE_TEARCOLOR and PerR_Data[data.RP_Index].CurrentRamsesTier >= 4 then
			player.TearColor = Color(0.4, 1, 0.4, 1, 0, 0, 0)
		end
		
		-- If the current Ramses Succ count is capped at Tier 4.
		if cacheFlag == CacheFlag.CACHE_FIREDELAY and PerR_Data[data.RP_Index].SuccedBonus >= 35 then
			player.MaxFireDelay = player.MaxFireDelay - 2
		end
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, Ramsesmod.UpdateStats)

function Ramsesmod:PostPlayerInit(player) --Give Ramses his starting items when the game starts
	-- If the player is Ramses
	if player:GetPlayerType() == RamsesPlayer then
		-- Add Pica and Gulp
		local GulpPill = Isaac.AddPillEffectToPool(PillEffect.PILLEFFECT_GULP) 
		player:AddCollectible(PicaItem, 0, false)
		player:AddPill(GulpPill)
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, Ramsesmod.PostPlayerInit)

local function ShutRamsesMouth(player, data) -- Function to hide Pica.
	if data.LiftingPica then
		data.LiftingPica = false
		player.Mass = 5
	
		if player:GetPlayerType() == RamsesPlayer then -- If the player is Ramses.
			sound:Play(SoundEffect.SOUND_GHOST_SHOOT, 1, 0, false, 1)
			player:AnimateCollectible(PicaItem, "HideItem", "Empty") -- Hide the item.
			
			if data.HungryRamses then -- Checks if the player has a Hungry Ramses head.
				data.HungryRamses:Remove()
				data.HungryRamses = nil
			end
		else -- If the player is not Ramses.
			sound:Play(SoundEffect.SOUND_CHEST_OPEN, 1, 0, false, 1)
			sound:Play(SoundEffect.SOUND_METAL_BLOCKBREAK, 1, 0, false, 1)
			player:AnimateCollectible(PicaItem, "HideItem", "PlayerPickup") -- Hide the item.
		end
		
		if data.PicaPullEffect then
			data.PicaPullEffect:Remove()
			data.PicaPullEffect = nil
		end
	end
end

function Ramsesmod:onNewRoom()
	for p = 0, game:GetNumPlayers() - 1 do
		local player = game:GetPlayer(p)
		-- Hides Pica if the player was lifting it (data wise)
		if player:HasCollectible(PicaItem) then
			ShutRamsesMouth(player, player:GetData())
		end
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, Ramsesmod.onNewRoom)

function Ramsesmod:OnDamage(ent, amt, flgs, src, cntdwn)
	local player = ent:ToPlayer()
	local data = player:GetData()
	
	if player:HasCollectible(PicaItem) then
		if PerR_Data[data.RP_Index].SuccedBonus >= 1 and player:GetPlayerType() == RamsesPlayer then  -- If the player is Ranses and has succed at least one pickup.
			-- Create a localized explosion
			local puke = player:FireBomb(player.Position, Vector(0, 0)) --=?= FireBomb() actually uses the player's tear flags, in comparison to Isaac.Spawn()
			puke.Visible = false
			puke:SetExplosionCountdown(1)
		end
		-- Takes 40% of your counter treshold or rounds it down to the player's current rank's minAmount.
		local SuccDecrease = math.floor((PerR_Data[data.RP_Index].MaxAmount - PerR_Data[data.RP_Index].MinAmount) * 0.40)
		PerR_Data[data.RP_Index].SuccedBonus = math.max(PerR_Data[data.RP_Index].SuccedBonus - SuccDecrease, PerR_Data[data.RP_Index].MinAmount)
		
		-- Update the player's stats.
		data.succTearBonus = false
		player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_FIREDELAY)
		player:EvaluateItems()
		
		ShutRamsesMouth(player, data)
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, Ramsesmod.OnDamage, EntityType.ENTITY_PLAYER)

function Ramsesmod:useItem(itemID)
	for p = 0, game:GetNumPlayers() - 1 do
		local player = game:GetPlayer(p)
		local data = player:GetData()
		
		-- Checks if the player pressed the Active or Card (Double active card) button, for true coop compatibility.
		if player:GetActiveItem() == itemID and (Input.IsActionTriggered(ButtonAction.ACTION_ITEM, player.ControllerIndex) or Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, player.ControllerIndex)) then
			if data.LiftingPica ~= true then
				data.LiftingPica = true
				player.Mass = 999 -- To make Ramses unable to get pushed by stuff, while Pica is lifted.
				
				if player:GetPlayerType() == RamsesPlayer then -- If the player is Ramses.
					player:AnimateCollectible(PicaItem, "LiftItem", "Empty") -- Lift up the item.
					sound:Play(SoundEffect.SOUND_LOW_INHALE, 1, 0, false, 1)
					
					-- Spawns the Ramses head effect.
					data.HungryRamses = Isaac.Spawn(EntityType.ENTITY_EFFECT, RamsesHead, 45, player.Position, Vector(0, 0), player):ToEffect()
					data.HungryRamses.RenderZOffset = 37935 -- This makes the head render on top of the player.
					data.HungryRamses.SpriteScale = player.SpriteScale * 1.1 -- Makes the head slightly bigger than the player's.
				else -- If the player is not Ramses.
					sound:Play(SoundEffect.SOUND_UNLOCK00, 1, 0, false, 1)
					sound:Play(SoundEffect.SOUND_METAL_BLOCKBREAK, 1, 0, false, 1)
					player:AnimateCollectible(PicaItem, "LiftItem", "PlayerPickup") -- Lift up the item.
				end
				
				-- Spawns the pulling effect.
				data.PicaPullEffect = Isaac.Spawn(EntityType.ENTITY_EFFECT, RamsesPullEffect, 45, player.Position, Vector(0, 0), player):ToEffect()
			else
				ShutRamsesMouth(player, data) -- Hides Pica.
			end
		end
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_USE_ITEM, Ramsesmod.useItem, PicaItem)

function Ramsesmod:UsePicaPull(player)
	local data = player:GetData()
	
	if data.TrueCoop and data.TrueCoop.PlayerListIndex then
		data.RP_Index = data.TrueCoop.PlayerListIndex -- To update the player index.
	end
	
	-- This pulls pickups and items towards Isaac
	if data.LiftingPica == true then
		--=?= FindInRadius(Vector(), 1500, X) [the 1500 bit], covers the entire room.
		for _, ent in pairs(Isaac.FindInRadius(player.Position, 1500, 62)) do -- Checks for everything in the room except effects.
			if ent.Type ~= 5 and ent.Type ~= 2 then -- Checks if the entity is not a pickup.
				ent.Velocity = ent.Velocity * 0.9 + (player.Position - ent.Position):Resized(1)
			else -- If the entity is a Pickup.
				ent.Velocity = ent.Velocity * 0.9 + (player.Position - ent.Position):Resized(0.5 + (PerR_Data[data.RP_Index].CurrentRamsesTier * 0.5))
			end
		end
		
		-- If the player is not at max succ count.
		if PerR_Data[data.RP_Index].SuccedBonus < PerR_Data[data.RP_Index].MaxAmount then
			-- Checks if the pickup is close enough o the player.
			for _, ent in pairs(Isaac.FindInRadius(player.Position, player.Size * 3, EntityPartition.PICKUP)) do
				if ent:ToPickup():IsShopItem() ~= true -- Checks if it's not a shop item,
				and(ent.Variant == PickupVariant.PICKUP_BOMB -- This is unnecessarily indentated, just wrote it like this to make it more readable.
					or ent.Variant == PickupVariant.PICKUP_COIN
					or ent.Variant == PickupVariant.PICKUP_HEART
					or ent.Variant == PickupVariant.PICKUP_KEY
					or ent.Variant == PickupVariant.PICKUP_LIL_BATTERY
					or ent.Variant == PickupVariant.PICKUP_PILL
					or ent.Variant == PickupVariant.PICKUP_TAROTCARD)
				then -- If the pickup is one of these specific ones.
					sound:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1)
					Isaac.Spawn(1000, 88, 150, ent.Position, Vector(0, 0), nil) -- Poof effect
					PerR_Data[data.RP_Index].SuccedBonus = math.min(PerR_Data[data.RP_Index].SuccedBonus + 1, PerR_Data[data.RP_Index].MaxAmount) -- Increase succ count.
					
					player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_FIREDELAY)
					player:EvaluateItems()
					ent:Remove()
				end
			end
		end
		
		-- Code to make the player eat troll bombs and flies.
		if PerR_Data[data.RP_Index].CurrentRamsesTier >= 3 then
			-- Checks if the entity is close enough o the player.
			for _, ent in pairs(Isaac.FindInRadius(player.Position, player.Size * 3, EntityPartition.ENEMY)) do
				if (ent.Type == EntityType.ENTITY_BOMBDROP and (ent.Variant == BombVariant.BOMB_TROLL or ent.Variant == BombVariant.BOMB_SUPERTROLL)) or ent.Type == EntityType.ENTITY_FLY then
					sound:Play(SoundEffect.SOUND_VAMP_GULP, 1, 0, false, 1)
					Isaac.Spawn(1000, 88, 150, ent.Position, Vector(0, 0), nil) -- Poof effect
					PerR_Data[data.RP_Index].SuccedBonus = math.min(PerR_Data[data.RP_Index].SuccedBonus + 2, PerR_Data[data.RP_Index].MaxAmount) -- Increases succ count by 2 instead of 1.
					
					player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_FIREDELAY)
					player:EvaluateItems()
					ent:Remove()
				end
			end
		end
	end
	
	-- If the player uses the card button (because cards interrupt the pickup animation)
	if data.LiftingPica and Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, player.ControllerIndex) then
		ShutRamsesMouth(player, data) -- To hide Pica.
	end
	
	-- To manage the Hungry Ramses effect.
	if data.HungryRamses then
		local RSprite = data.HungryRamses:GetSprite()
		if RSprite:IsFinished("Appear") then
			RSprite:Play("Idle", false)
		end
		
		data.HungryRamses.Velocity = player.Position - data.HungryRamses.Position -- Puts the head on top of the player.
		data.HungryRamses:SetColor(player:GetColor(), -1, 50, false, false) -- Changes the color of the head to the player's.
		data.HungryRamses:SetTimeout(2) -- Makes the effect last indefinitely, until stated otherwise.
	end
	
	-- To manage the Pica Pulling effect.
	if data.PicaPullEffect then
		data.PicaPullEffect.Velocity = player.Position - data.PicaPullEffect.Position -- Puts the pulling effect on top of the player.
		data.PicaPullEffect:SetTimeout(2) -- Makes the effect last indefinitely, until stated otherwise.
	end
	
	if player:HasCollectible(PicaItem) or player:GetPlayerType() == RamsesPlayer then
		-- Change the player's color when at max counter.
		if PerR_Data[data.RP_Index].SuccedBonus >= PerR_Data[data.RP_Index].MaxAmount then
			if PerR_Data[data.RP_Index].SuccedBonus < 35 then
				-- Used for the special gradient effect.
				data.RedV = data.RedV or 1
			
				if data.RedV <= 0.55 then -- Revers from black to red.
					data.Invert = true
				elseif data.RedV >= 0.95 then -- Inverts from red to black.
					data.Invert = false
				end
				
				-- Used to slowly increase/decrease the gradient.
				if data.Invert then
					data.RedV = data.RedV + 0.025
				else
					data.RedV = data.RedV - 0.025
				end
				
				player:SetColor(Color(data.RedV, 0, 0, 1, 0, 0, 0), -1, 50, false, false)
			else
				-- Used to set the player's color to red when at max counter on tier 4.
				player:SetColor(Color(1, 0, 0, 1, 0, 0, 0), -1, 50, false, false)
			end
		end
	end
	
	if player:HasCollectible(PicaItem) ~= true then -- To remove the Pica bonus when the player drops Pica.
		if not data.HasUpdatedPica then
			data.HasUpdatedPica = true
			PerR_Data[data.RP_Index].SuccedBonus = PerR_Data[data.RP_Index].MinAmount
			
			-- To update the player's stats.
			player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_SPEED | CacheFlag.CACHE_TEARFLAG | CacheFlag.CACHE_TEARCOLOR | CacheFlag.CACHE_FIREDELAY)
			player:EvaluateItems()
		end
	else
		if data.HasUpdatedPica ~= nil then
			data.HasUpdatedPica = nil
			
			-- To update the player's stats.
			player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_SPEED | CacheFlag.CACHE_TEARFLAG | CacheFlag.CACHE_TEARCOLOR | CacheFlag.CACHE_FIREDELAY)
			player:EvaluateItems()
		end
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, Ramsesmod.UsePicaPull)

-- Code to make Pica level up
function Ramsesmod:onPlayerRender(player)
	local data = player:GetData()
	if Input.IsActionTriggered(ButtonAction.ACTION_DROP, player.ControllerIndex) and PerR_Data[data.RP_Index].CurrentRamsesTier then
		if PerR_Data[data.RP_Index].SuccedBonus >= PerR_Data[data.RP_Index].MaxAmount and PerR_Data[data.RP_Index].CurrentRamsesTier < 4 then
			-- To update the player's Pica status.
			PerR_Data[data.RP_Index].CurrentRamsesTier = PerR_Data[data.RP_Index].CurrentRamsesTier + 1
			PerR_Data[data.RP_Index].MinAmount = RamsesTiers[PerR_Data[data.RP_Index].CurrentRamsesTier].MinAmount
			PerR_Data[data.RP_Index].MaxAmount = RamsesTiers[PerR_Data[data.RP_Index].CurrentRamsesTier].MaxAmount
			PerR_Data[data.RP_Index].SuccedBonus = PerR_Data[data.RP_Index].MinAmount
			
			-- To update the player's stats on level up.
			player:AddCacheFlags(CacheFlag.CACHE_DAMAGE | CacheFlag.CACHE_SPEED | CacheFlag.CACHE_TEARFLAG | CacheFlag.CACHE_TEARCOLOR| CacheFlag.CACHE_FIREDELAY)
			player:EvaluateItems()
			
			if player:GetPlayerType() == RamsesPlayer then -- If the player is Ramses,
				sound:Play(SoundEffect.SOUND_MONSTER_ROAR_2, 1, 0, false, 1)
			else -- If the player is not Ramses.
				sound:Play(SoundEffect.SOUND_POWERUP_SPEWER, 1, 0, false, 1)
			end
		end
	end
end
Ramsesmod:AddCallback(ModCallbacks.MC_POST_PLAYER_RENDER, Ramsesmod.onPlayerRender)

--code to make bombs unlock key doors
function Ramsesmod:KeyBombs(eff)
	if eff.FrameCount <= 1 then -- If the effect just spawned (effect init doesn't return a Position, so it's no use)
		for p = 0, game:GetNumPlayers() - 1 do
			local player = game:GetPlayer(p)
			
			if player:GetPlayerType() == RamsesPlayer then
				local room = Game():GetRoom()
				
				for i = 0, 7 do
					local door = room:GetDoor(i)
					if door ~= nil and door:IsLocked() then
						-- If the bomb is close enough to a door.
						if eff.Position:DistanceSquared(door.Position) <= 55 ^ 2 then --=?= :DistanceSquared() is about 5 times more efficient than :Distance()
							if door:TryUnlock(true) then
								door:SpawnDust()
							end
						end
					end
				end
			end
		end
    end
end
Ramsesmod:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, Ramsesmod.KeyBombs, EffectVariant.BOMB_EXPLOSION)